This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## How to start

1. Run `npm install`
2. Run `npm start`

## Instructions

1. Read the file which describes the assignment.
2. Clone this repository to your computer.
3. Create a branch with your name, like `your-first-and-last-name` (example `mary-smith`).
4. Follow the 2 steps in the above section **How to start** in this document to see your initial react app up and running.
5. Follow the instructions in the assignment file.


